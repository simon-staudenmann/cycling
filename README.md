# Cycling

This repositories stores the files used for network analysis of the Cycling Hire Trip Data provided as open data by Transport for London (TfL).

## Getting Started

`git`, `make` and `python` with the packages listed in `environment.yml` should be installed on your local machine. Then execute the following commands to clone the environment and generate the dataset used for analysis.

```bash
git clone git@gitlab.com:simon_/cycling.git
cd cycling
make data/final/trips.gml
```

## Directory Structure

- `analysis/` holds output of long running calculations that were done while analyzing the network. Scripts that generated that output can be found in `src/analysis/`and `src/modularity/`.
- `data/` contains three directories which make up the data processing pipeline for generating the final dataset used for analysis.
- `notebooks/` holds clean notebooks used to analyze the network.
- `src/analysis/` holds long-running scripts to calculate distances between bikepoints and the model that creates a network similar to the one we analyzed.
- `src/data/` holds the scripts for the data processing pipeline.
- `src/modularity/` holds a gradle project that uses Gephi Toolkit to do a grid search to calculate the modularity with different parameters and a script to calculate the modularity classes for every node based on a given parameter configuration.