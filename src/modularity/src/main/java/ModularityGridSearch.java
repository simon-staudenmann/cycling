import com.opencsv.CSVWriter;
import org.gephi.appearance.api.*;
import org.gephi.appearance.plugin.PartitionElementColorTransformer;
import org.gephi.graph.api.Column;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.statistics.plugin.Modularity;
import org.openide.util.Lookup;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ModularityGridSearch {

    private static void execute(String in, String out) throws IOException {

        Initializer.initialize(in);

        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
        DirectedGraph graph = graphModel.getDirectedGraph();
        AppearanceController appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
        AppearanceModel appearanceModel = appearanceController.getModel();

        List<String[]> result = new LinkedList<>();

        for (double i = 0; i < 5; i += 0.1) {
            for (int k = 0; k < 2; k++) {
                String[] row = new String[5];

                //Run modularity algorithm - community detection
                Modularity modularity = new Modularity();
                modularity.setResolution(i);
                modularity.setUseWeight(k % 2 == 0);
                modularity.execute(graphModel);

                row[0] = String.format("%.1f", modularity.getResolution());
                row[1] = Boolean.toString(modularity.getRandom());
                row[2] = Boolean.toString(modularity.getUseWeight());
                row[3] = String.format("%.5f", modularity.getModularity());

                //Partition with 'modularity_class', just created by Modularity algorithm
                Column modColumn = graphModel.getNodeTable().getColumn(Modularity.MODULARITY_CLASS);
                Function function = appearanceModel.getNodeFunction(graph, modColumn, PartitionElementColorTransformer.class);
                Partition partition = ((PartitionFunction) function).getPartition();

                row[4] = Integer.toString(partition.size());

                result.add(row);
            }
        }

        CSVWriter writer = new CSVWriter(new FileWriter(out));
        writer.writeNext(new String[]{"resolution", "random", "useWeight", "modularity", "components"});
        writer.writeAll(result);
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        execute(args[0], args[1]);
    }
}