import com.opencsv.CSVWriter;
import org.gephi.graph.api.*;
import org.gephi.statistics.plugin.Modularity;
import org.openide.util.Lookup;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class NodesModularityClass {

    public static void execute(String in, String out, boolean randomize, boolean useEdgeWeight, double resolution) throws IOException {

        Initializer.initialize(in);

        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
        DirectedGraph graph = graphModel.getDirectedGraph();

        Modularity modularity = new Modularity();


        //Run modularity algorithm - community detection
        modularity.setRandom(randomize);
        modularity.setUseWeight(useEdgeWeight);
        modularity.setResolution(resolution);
        modularity.execute(graphModel);

        List<String[]> result = new LinkedList<>();
        for (Node node : graph.getNodes()) {
            result.add(new String[]{node.getLabel(), Integer.toString((Integer) node.getAttribute(Modularity.MODULARITY_CLASS))});
        }

        CSVWriter writer = new CSVWriter(new FileWriter(out));
        writer.writeNext(new String[]{"node", "modularity_class"});
        writer.writeAll(result);
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        execute(args[0], args[1], Boolean.parseBoolean(args[2]), Boolean.parseBoolean(args[3]), Double.parseDouble(args[4]));
    }
}
