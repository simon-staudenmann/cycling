import pandas as pd
import networkx as nx
from geopy.distance import distance
from tqdm import tqdm
from random import random, randint
from scipy.stats import poisson
import pickle

bikepoints = pd.read_pickle('../data/intermediate/filtered_bikepoints.pickle')

with open('../../data/intermediate/distance_lookup.pickle', 'rb') as f:
    distances_lookup = pickle.load(f)

pois = pd.read_pickle('....//data/intermediate/points_of_interest.pickle')
hyde_park = pois[pois['pointOfInterest'] == 'Hyde Park']

bikepoints['distance'] = bikepoints.apply(lambda row: round(distance((hyde_park['lat'].values[0], hyde_park['lon'].values[0]), (row['lat'], row['lon'])).km), axis=1)

F = nx.MultiDiGraph()

F.add_nodes_from(bikepoints['id'])
nx.set_node_attributes(F, bikepoints.set_index('id').to_dict(orient='index'))

i = 0
pbar = tqdm(total=1029716)
while i < 1029716:
    a = randint(0, 781)
    start = bikepoints.iloc[a]

    while random() > poisson.pmf(start['distance'], mu=4.094):
        a = randint(0, 781)
        start = bikepoints.iloc[a]

    b = randint(0, 781)
    end = bikepoints.iloc[b]
    while random() > poisson.pmf(start['distance'], mu=4.094):
        b = randint(0, 781)
        end = bikepoints.iloc[b]

    d = distances_lookup[(start['id'], end['id'])]
    if random() < poisson.pmf(round(d), mu=2.119):
        F.add_edge(start['id'], end['id'])
        i += 1
        pbar.update(1)
        
pbar.close()
nx.write_gml(F, '../../analysis/generated_trips.gml')