import pandas as pd

import networkx as nx

import pickle

from geopy.distance import distance
from tqdm import tqdm


G = nx.read_gml('../../data/final/trips.gml')

bikepoints = pd.read_pickle('../../data/intermediate/filtered_bikepoints.pickle')

pois = pd.read_pickle('../../data/intermediate/points_of_interest.pickle')
hyde_park = pois[pois['pointOfInterest'] == 'Hyde Park']

bikepoints['distance'] = bikepoints.apply(lambda row: distance((hyde_park['lat'].values[0], hyde_park['lon'].values[0]), (row['lat'], row['lon'])).km, axis=1)

stations = []
for i, j, k in G.edges:
    stations.append(int(i))
    stations.append(int(j))

distances = []
for i in tqdm(stations):
    distances.append(bikepoints[bikepoints['id'] == i]['distance'].values[0])

with open('../../analysis/distances_to_hyde.pickle', 'wb') as file:
    pickle.dump(distances, file)