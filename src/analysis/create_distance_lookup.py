import click
import pandas as pd
import pickle

from geopy.distance import distance


@click.command()
@click.option('--bikepoints')
@click.option('--lookup')
def create_distance_lookup(bikepoints, lookup):
    bikepoints = pd.read_pickle(bikepoints)

    distance_lookup = {}
    for i in bikepoints['id']:
        for j in bikepoints['id']:
            if i == j:
                distance_lookup[(i, j)] = 0
            elif (j, i) in distance_lookup:
                distance_lookup[(i, j)] = distance_lookup[(j, i)]
            else:
                start = bikepoints[bikepoints['id'] == i].iloc[0]
                end = bikepoints[bikepoints['id'] == j].iloc[0]
                d = distance((start['lat'], start['lon']),
                             (end['lat'], end['lon'])).km
                distance_lookup[(i, j)] = d

    with open(lookup, 'wb') as file:
        pickle.dump(distance_lookup, file)


if __name__ == "__main__":
    create_distance_lookup()
