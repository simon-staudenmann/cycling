.PHONY: clean

data = data/
raw = $(data)raw/
int = $(data)intermediate/
fin = $(data)final/

clean:
	rm -f $(raw)*
	rm -f $(int)*
	rm -f $(fin)*

$(raw)trips.csv:
	curl https://cycling.data.tfl.gov.uk/usage-stats/121JourneyDataExtract01Aug2018-07Aug2018.csv > $(raw)trips.csv
	curl https://cycling.data.tfl.gov.uk/usage-stats/122JourneyDataExtract08Aug2018-14Aug2018.csv | sed 1d >> $(raw)trips.csv
	curl https://cycling.data.tfl.gov.uk/usage-stats/123JourneyDataExtract15Aug2018-21Aug2018.csv | sed 1d >> $(raw)trips.csv
	curl https://cycling.data.tfl.gov.uk/usage-stats/124JourneyDataExtract22Aug2018-28Aug2018.csv | sed 1d >> $(raw)trips.csv
	curl https://cycling.data.tfl.gov.uk/usage-stats/125JourneyDataExtract29Aug2018-04Sep2018.csv | sed 1d >> $(raw)trips.csv

$(raw)bikepoints.json:
	curl https://api.tfl.gov.uk/BikePoint > $(raw)bikepoints.json

$(raw)points_of_interest.html:
	curl -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' 'https://www.google.com/destination/map/topsights?q=london&dest_mid=/m/04jpl' > data/raw/points_of_interest.html

$(int)trips.pickle: src/data/parse_trips.py $(raw)trips.csv
	python src/data/parse_trips.py --raw=$(raw)trips.csv --intermediate=$(int)trips.pickle

$(int)bikepoints.pickle: src/data/parse_bikepoints.py $(raw)bikepoints.json
	python src/data/parse_bikepoints.py --raw=$(raw)bikepoints.json --intermediate=$(int)bikepoints.pickle

$(int)filtered_trips.pickle $(int)filtered_bikepoints.pickle: src/data/filter.py $(int)trips.pickle $(int)bikepoints.pickle
	python src/data/filter.py --trips=$(int)trips.pickle --bikepoints=$(int)bikepoints.pickle --filtered_trips=$(int)filtered_trips.pickle --filtered_bikepoints=$(int)filtered_bikepoints.pickle

$(int)distance_lookup.pickle: src/analysis/create_distance_lookup.py $(int)filtered_bikepoints.pickle
	python src/analysis/create_distance_lookup.py --bikepoints=$(int)filtered_bikepoints.pickle --lookup=$(int)distance_lookup.pickle

$(int)points_of_interest.pickle: src/data/parse_points_of_interest.py data/raw/points_of_interest.html
	python src/data/parse_points_of_interest.py --raw=$(raw)points_of_interest.html --intermediate=$(int)points_of_interest.pickle

$(fin)trips.gml: src/data/create_gml.py $(int)filtered_trips.pickle $(int)filtered_bikepoints.pickle
	python src/data/create_gml.py --trips=$(int)filtered_trips.pickle --bikepoints=$(int)filtered_bikepoints.pickle --gml=$@